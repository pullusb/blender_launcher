# Blender launcher
Pyside2 Gui to download and launch chosen version of blender.

Modified version of [Christophe Seux launcher](https://gitlab.com/ChristopheSeux/launcher) code.

**[Download latest](https://gitlab.com/pullusb/blender_launcher/-/archive/master/blender_launcher-master.zip)**

<!-- ### [Demo Youtube]() -->

**/!\ WIP**

To get it to run you need to install those 3 modules with pip:  
`PySide2`, `PyYAML`, `yamlordereddictloader`
  
`pip install PySide2 PyYAML yamlordereddictloader` (untested, I did one by one)

If you need to specify a python version:  
windows: `py -3.7 -m pip install PySide2`  
linux: `python3.7 -m pip install PySide2`  

Then execute `Launcher.py`.  
Currently the installation folder is the same as the script folder.  
Blender(s) instance(s) launched will use the console of the Launcher (carefull not to close accidentally)

## Todo:
  - to bundle with pyinstaller, avoid call to another file, import instead.
  - add option to specify install directory
  - add option do avoid download check and just launch.

---

## Changelog:
  2020-04-04 v0.1.0:
  - initial version