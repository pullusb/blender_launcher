import os,shutil
from os.path import join, basename, exists, dirname
import subprocess
import urllib.request
import ssl
import re
import zipfile
import bz2
import tempfile
import tarfile
import json
import sys


### /DL utils

from sys import platform

def detect_OS():
    """return str of os name : Linux, Windows, Mac (None if undetected)"""
    myOS = platform

    if myOS.startswith('linux') or myOS.startswith('freebsd'):# linux
        print("operating system : Linux")
        return ("Linux")

    elif myOS.startswith('win'):# Windows
        print("operating system : Windows")
        return ("Windows")

    elif myOS == "darwin":# OS X
        print("operating system : Mac")
        return ('Mac')

    else:# undetected
        print("Cannot detect OS, python 'sys.platform' give :", myOS)
        return None


def sizeof_fmt(num, suffix='B'):
    '''
    return human readable size from bytes amount
    ex: sizeof_fmt(os.stat(filename).st_size)
    '''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def copyfileobjverbose(fsrc, fdst, size=None, length=16*1024):
    '''
    exactly like shutil.copyfileobj with a callback
    copy data from file-like object fsrc to file-like object fdst
    '''
    copied = 0
    while True:
        buf = fsrc.read(length)
        if not buf:
            break
        fdst.write(buf)
        copied += len(buf)
        if size:
            print(f'\rdownloaded: {sizeof_fmt(copied)} ({int((copied/size)*100)}%)    ', end='')
        else:
            print(f'\rdownloaded: {sizeof_fmt(copied)}', end='')
    print()#print last carriage return


def dl_url(url, dest):
    import urllib.request
    import time
    '''download passed url as dest file (include,filename)'''
    start_time = time.time()
    ## simple download
    # with urllib.request.urlopen(url) as response, open(dest, 'wb') as out_file:
    #     shutil.copyfileobj(response, out_file)

    ## with displayed progression (need sizeof_fmt and copyfileobjverbose func)
    with urllib.request.urlopen(url) as response, open(dest, 'wb') as out_file:
        clength = response.getheader('content-length')#check if size available in header
        if clength: clength = int(clength)
        copyfileobjverbose(response, out_file, size=clength)

    print(f"elapsed time {time.time() - start_time:.2f}s",)

### DL utils/


args = json.loads(sys.argv[-1])
version = args['version']

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
# TODO maybe use a passed argument for location instead of program location
softs_dir = ''
if not softs_dir or not exists(softs_dir):
    #local
    softs_dir = os.getcwd()#'/opt'

# print('softs_dir getcwd: ', softs_dir)
# print('dirname(__file__):', dirname(__file__))
#if not exists(softs_dir) :  os.mkdir(softs_dir)

my_os = detect_OS()


# Check for existing release

if version != 'Last Build' :
    url = f'https://download.blender.org/release/Blender{version}'

    print(url)
    response = urllib.request.urlopen(url, context = ssl_context)
    data = response.read()
    body = data.decode("utf-8")
    
    if my_os == "Linux":
        match = re.findall(r'<a href=\"(.*linux.*)">', body)
        blist = [m for m in match if 'x86_64' in m and not re.search(r'rc\d',m)]
        if not blist:
            blist = [m for m in match if '64' in m and not re.search(r'rc\d',m)] #or linux64 seems to be the new notation (linux already matched before)
    
    elif my_os == "Windows":
        match = re.findall(r'<a href=\"(.*windows64\.zip.*)">', body)
        blist = [m for m in match if '64' in m and not re.search(r'rc\d',m)]

    if not blist:
        print(f'no matching name in page body :\n{body}')
        
    bname = blist[-1]

    real_version = re.findall(r'\d\.\d{2}[a-z]?', bname)[0]

    blender_dir_name = 'blender-%s'%real_version
    blender_path = join(softs_dir,blender_dir_name,'blender')
    blender_dir = join(softs_dir,blender_dir_name)
    if not exists(blender_path) :
        download_url = url+'/' + bname

        print('download_url',download_url)
        download_path = join(tempfile.gettempdir(), basename(download_url))
        dl_url(download_url, download_path)

        if my_os == "Linux": 
            with tarfile.open(download_path) as tar:#, "r:bz2"
                tar.extractall(softs_dir)
                shutil.move(join(softs_dir,basename(download_path).replace('.tar.bz2','').replace('.tar.xz','')),blender_dir)
        else:
            with zipfile.ZipFile(download_path) as zp:
                zp.extractall(softs_dir)
                shutil.move(join(softs_dir,basename(download_path).replace('.zip','')),blender_dir)

# Get last build
elif version == 'Last Build' :
    burl = "https://builder.blender.org/"
    response = urllib.request.urlopen(burl,context= ssl_context)
    data = response.read()
    body = data.decode("utf-8")
    ## ex : /download/blender-2.83-a322b43e3d08-linux64.tar.xz

    ## old (bz2):  #/download/blender-\d\.\d{2}-(.{4,20})-linux-.{2,20}-x86_64.tar.bz2
    ## old (-x86_[64]) : /download/blender-\d\.\d{2}-(\w{4,20})-linux-\w{2,20}-x86_64\.tar\.\w+

    match = None
    if my_os == "Linux":
        match = re.search(r'/download/blender-\d\.\d{2}-(\w{4,20})-linux(?:-\w{2,20}-x86_)?64\.tar\.\w+',body)
    else:#elif my_os == "Windows":
        match = re.search(r'/download/blender-\d\.\d{2}-(\w{4,20})-windows(?:-\w{2,20}-x86_)?64\.zip',body)

    if not match:
        print(10*'=', '\nRegex failed to match download blender line on https://builder.blender.org/\n', 10*'=', '\npage code :\n', body, 10*'=')
    hash = match.group(1)
    download_url = burl + match.group(0)
    print('hash',hash)


    real_version = re.findall(r'\d\.\d{2}[a-z]?', match.group(0))[0]

    blender_dir_name = 'blender-%s'%real_version
    blender_path = join(softs_dir,blender_dir_name,'blender')
    blender_dir = join(softs_dir,blender_dir_name)


    current_hash = ''
    if exists(blender_path) :
        process = subprocess.Popen([blender_path,'--version'],stdout = subprocess.PIPE)
        stdout = process.communicate()
        stdout = stdout[0].decode('utf-8').strip('\n').split('\n')

        #version = stdout[0]
        bl_info = {k.strip('\t').replace(' ','_'):v.strip() for k,v in [i.split(':',1) for i in stdout[1:]]}

        current_hash = bl_info['build_hash']
        print('current_hash',current_hash)


    if current_hash != hash :
        print('Check for the new buildbot version')

        blender_dir = join(softs_dir,blender_dir_name)
        if os.path.exists(blender_dir) :
            print('Remove blender dir')
            shutil.rmtree(blender_dir)

        print('tempfile.gettempdir(): ', tempfile.gettempdir())
        download_path = join(tempfile.gettempdir(),basename(download_url))
        if not os.path.exists(download_path) :
            print('download_url',download_url)
            dl_url(download_url, download_path)

        ## UNZIP
        try :
            if my_os == "Linux": 
                with tarfile.open(download_path) as tar:#, "r:bz2"
                    tar.extractall(softs_dir)
                    shutil.move(join(softs_dir,basename(download_path).replace('.tar.bz2','').replace('.tar.xz','')),blender_dir)
            else:
                with zipfile.ZipFile(download_path) as zp:
                    zp.extractall(softs_dir)
                    shutil.move(join(softs_dir,basename(download_path).replace('.zip','')),blender_dir)
        except Exception as e :
            print('The download file is corrupted\n',e)
            print(f'removing and re-download {download_path}')
            os.remove(download_path)

            print('download_url',download_url)
            dl_url(download_url, download_path)



# Launch
subprocess.Popen(blender_path, shell=True)
